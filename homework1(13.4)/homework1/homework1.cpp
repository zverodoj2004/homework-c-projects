﻿#include"Helpers.h"

using namespace std;

int main()
{
	setlocale(LC_ALL, "rus");
	cout << "***Программа для нахождения квадрата суммы двух чисел***" << endl << endl;
	double a, b;
	cout << "Введите первое число a = ";
	cin >> a;
	cout << "Введите второе число b = ";
	cin >> b;
	cout << "Квадрат суммы чисел " << a << " и " << b << " = " << SquareSum(a, b) << endl;
}